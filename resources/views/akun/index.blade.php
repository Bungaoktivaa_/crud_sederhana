@extends('layout/main')
@section('title','Akun')
    
@section('container')

<div class="col-md-12">
<a href="/akun/create" class="btn btn-success my-3">Tambah Data</a>
    <div class="card">
    
        <div class="card-header">
            <strong class="card-title">Data</strong>
        </div>
        <div class="card-body">
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Password</th>
        <th>Foto</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($akun as $dkr)
          <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$dkr->nama}}</td>
            <td>{{$dkr->email}}</td>
            <td>{{$dkr->password}}</td>
            <td><img src="/post-images/{{$dkr->foto}}" height="50px" width="50px"> </td>
            <td>
              <a href="akun/{{$dkr->id}}/edit" class="btn btn-warning">Edit</a>
              <form action="akun/{{$dkr->id}}" method="post" class="d-inline">
                @method('delete')
                @csrf
              
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </td>
          </tr>
          @endforeach
        </tbody>
  </table>
        </div>
    </div>
</div>
</div>

@endsection