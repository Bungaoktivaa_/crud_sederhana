@extends('layout/main')
@section('title','Edit Data')

@section('container')
<div class="col-lg-6">
    <div class="card">
        <form method="post" action="/akun/{{$akun->id}}" enctype="multipart/form-data">
            @method('patch')
                @csrf
                <div class="card-header"><strong>Edit</strong><small> Data</small></div>
                    <div class="card-body card-block">
                        <div class="mb-3">
                          <label class="form-label">Nama</label>
                          <input type="text" name="nama" value="{{$akun->nama}}" class="form-control @error('nama') is-invalid @enderror" >
                          @error('nama')
                              <div class="invalid-feedback{{$message}}"></div>
                          @enderror
                          <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                        </div>
                       
                        <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" name="email" value="{{$akun->email}}" class="form-control  @error('email') is-invalid @enderror" >
                            @error('email')
                              <div class="invalid-feedback{{$message}}"></div>
                            @enderror
                            <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                        </div>
                        
                        <div class="mb-3">
                            <label class="form-label">Password</label>
                            <input type="text" name="password" value="{{$akun->password}}" class="form-control @error('password') is-invalid @enderror">
                            @error('password')
                              <div class="invalid-feedback{{$message}}"></div>
                            @enderror
                            <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                        </div>
                        @if($akun->foto)
                          <img src="/post-images/{{$akun->foto}}" width="75px" height="75px">
                        @else
                          <p>Tidak Ada Photo</p>
                        @endif
                        <div class="mb-3">
                          <label class="form-label">Foto</label>
                          <input type="file" name="foto" value="{{$akun->foto}}" class="form-control @error('foto') is-invalid @enderror">
                          @error('foto')
                            <div class="invalid-feedback{{$message}}"></div>
                          @enderror
                          <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                      </div>
                     
                       
                        
                        <button type="submit" class="btn btn-primary">Update Data</button>
                        <a href="/akun" class="btn btn-danger">Kembali</a>
                    </form>
                </div>
                </div>

@endsection