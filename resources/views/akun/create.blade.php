@extends('layout/main')
@section('title','Tambah Data')

@section('container')
<div class="col-lg-10">
    <div class="card">
            <form method="post" action="/akun" enctype="multipart/form-data">
                <div class="card-title">
                    <h3 class="text-center">AKUN</h3>
                </div>
                <hr>
                @csrf
                <div class="card-header"><strong>Tambah</strong><small> Data</small></div>
                <div class="card-body card-block">
                    <div class="mb-3">
                        <label class="form-label">Nama</label>
                        <input type="text" name="nama" value="{{old('nama')}}" class="form-control @error('nama') is-invalid @enderror" placeholder="Masukan Nama">
                        @error('nama')
                            <div class="invalid-feedback{{$message}}"></div>
                        @enderror
                        <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                      </div>
                      <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="email" name="email" value="{{old('email')}}" class="form-control @error('email') is-invalid @enderror" placeholder="Masukan Nama email">
                        @error('email')
                            <div class="invalid-feedback{{$message}}"></div>
                        @enderror
                        <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                      </div>
                      <div class="mb-3">
                        <label class="form-label">Password</label>
                        <input type="text" name="password" value="{{old('password')}}" class="form-control @error('password') is-invalid @enderror" placeholder="Masukan password">
                        @error('password')
                            <div class="invalid-feedback{{$message}}"></div>
                        @enderror
                        <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                      </div>

                      <div class="mb-3">
                        <label class="form-label">Foto</label>
                        <input type="file" name="foto" value="{{old('foto')}}" class="form-control @error('foto') is-invalid @enderror" placeholder="Masukan foto">
                        @error('foto')
                            <div class="invalid-feedback{{$message}}"></div>
                        @enderror
                        <!--<div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>-->
                      </div>


                    
                      
        
            <button type="submit" class="btn btn-primary">Tambah Data</button>
            <a href="/akun" class="btn btn-danger">Kembali</a>
        </form>
    </div>
    </div>
    
    @endsection