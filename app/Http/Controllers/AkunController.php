<?php

namespace App\Http\Controllers;

use App\Models\akun;
use Illuminate\Http\Request;

class AkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $akun=akun::all();        
        return view('akun/index', compact('akun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('akun/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        {
            $request->validate([
                'nama'=>'required',
                'email'=>'required',
                'password'=>'required',
                'foto'=>'image|file|max:2048|mimes:jpg,png,jpeg'
            ]);
    
            $imgName=null;
            if($request->foto){
                $imgName=$request->foto->getClientOriginalName() .'-' . time(). '-' . $request->foto->extension();    
                
                //$imgName=$request->foto->getClientOriginalName();
                $request->foto->move(public_path('post-images'),$imgName);
            }
    
    
            //produsen::create($request->all());
            akun::create([
                'nama'=>$request['nama'],
                'email'=>$request['email'],
                'password'=>$request['password'],
                'foto'=>$imgName
            ]);
    
            return redirect('/akun')->with('status','Data Berhasil Ditambahkan');
    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function show(akun $akun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function edit(akun $akun)
    {
        
        return view('akun/edit',compact('akun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, akun $akun)
    {
        $imgName=null;
        if($request->foto){
            $imgName=$request->foto->getClientOriginalName() .'-' . time(). '-' . $request->foto->extension();    
            
            //$imgName=$request->foto->getClientOriginalName();
            $request->foto->move(public_path('post-images'),$imgName);
        }

        akun::where('id',$akun->id)
        ->update(['nama'=>$request->nama,
                    'email'=>$request->email,
                    'password'=>$request->password,
                    'foto'=>$imgName]);
        return redirect('/akun')->with('status','Data Berhasil Diubah');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\akun  $akun
     * @return \Illuminate\Http\Response
     */
    public function destroy(akun $akun)
    {
        akun::destroy($akun->id);
        return redirect('akun')->with('status','Data Berhasil Dihapus');
    }
}
